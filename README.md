# jupyter notebooks

High quality jupyter notebook examples

## Frameworks

  - Tensorflow 1.15
  - Tensorflow 2.x
  - Pytorch
  - Fastai
  - MXNet
  - CNTK (Microsoft Cognitive Toolkit)
